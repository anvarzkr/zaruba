# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


if Role.count == 0
  ['registered', 'banned', 'moderator', 'admin'].each do |role|
    Role.find_or_create_by({name: role})
  end
end

if Type.count == 0
  [['Молодняк до 18', 300, false], ['Любители', 600, false], ['Профи', 1000, false], ['Девушки', 600, false], ['Команды', 2000, true]].each do |title, price, is_team|
    Type.find_or_create_by({title: title, price: price, beat_price: 1000, is_team: is_team})
  end
end

if User.count == 0
  User.create!({email: 'admin@admin.ru', fname: 'Admin', lname: 'Admin', password: 'admin@admin', password_confirmation: 'admin@admin', user_agreement: "1", phone: '89000000000', role_id: Role.find_by_name('admin').id})
end
