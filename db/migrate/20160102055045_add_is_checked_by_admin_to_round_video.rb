class AddIsCheckedByAdminToRoundVideo < ActiveRecord::Migration
  def change
    add_column :round_videos, :is_checked, :boolean, default: false
  end
end
