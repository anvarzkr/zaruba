class AddIsTeamToTypes < ActiveRecord::Migration
  def change
    add_column :types, :is_team, :boolean, default: false
  end
end
