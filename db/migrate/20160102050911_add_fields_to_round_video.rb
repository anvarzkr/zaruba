class AddFieldsToRoundVideo < ActiveRecord::Migration
  def change
    add_column :round_videos, :minutes, :integer, default: 0
    add_column :round_videos, :seconds, :integer, default: 0
    add_column :round_videos, :repeats, :integer, default: 0
  end
end
