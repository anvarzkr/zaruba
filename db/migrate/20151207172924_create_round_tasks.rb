class CreateRoundTasks < ActiveRecord::Migration
  def change
    create_table :round_tasks do |t|
      t.string :title
      t.belongs_to :round, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
