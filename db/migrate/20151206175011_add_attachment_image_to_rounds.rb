class AddAttachmentImageToRounds < ActiveRecord::Migration
  def self.up
    change_table :rounds do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :rounds, :image
  end
end
