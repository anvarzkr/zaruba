class AddRepeatsAndTimeToRounds < ActiveRecord::Migration
  def change
    add_column :rounds, :repeats, :integer, default: 0
    add_column :rounds, :minutes, :integer, default: 0
    add_column :rounds, :seconds, :integer, default: 0
  end
end
