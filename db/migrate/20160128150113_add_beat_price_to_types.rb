class AddBeatPriceToTypes < ActiveRecord::Migration
  def change
    add_column :types, :beat_price, :integer, default: 1000
  end
end
