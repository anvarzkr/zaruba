class AddUseImageToAds < ActiveRecord::Migration
  def change
    add_column :ads, :use_image, :boolean, default: false
  end
end
