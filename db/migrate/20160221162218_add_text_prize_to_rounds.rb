class AddTextPrizeToRounds < ActiveRecord::Migration
  def change
    add_column :rounds, :text_prize, :string
  end
end
