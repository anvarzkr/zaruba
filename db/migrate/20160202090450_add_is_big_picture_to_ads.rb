class AddIsBigPictureToAds < ActiveRecord::Migration
  def change
    add_column :ads, :is_big_picture, :boolean, default: false
  end
end
