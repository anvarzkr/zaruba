class AddCompetitionIdToRound < ActiveRecord::Migration
  def change
    add_reference :rounds, :competition, index: true, foreign_key: true
  end
end
