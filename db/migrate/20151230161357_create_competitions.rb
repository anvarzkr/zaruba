class CreateCompetitions < ActiveRecord::Migration
  def change
    create_table :competitions do |t|
      t.string :title
      t.text :description
      t.date :start_date
      t.integer :days, default: 7
      t.integer :timer_days, default: 3

      t.timestamps null: false
    end
  end
end
