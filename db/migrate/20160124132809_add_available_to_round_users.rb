class AddAvailableToRoundUsers < ActiveRecord::Migration
  def change
    add_column :round_users, :available, :integer, default: 1
  end
end
