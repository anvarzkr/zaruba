class CreateRoundVideos < ActiveRecord::Migration
  def change
    create_table :round_videos do |t|
      t.belongs_to :round_user, index: true, foreign_key: true
      t.string :youtube_url

      t.timestamps null: false
    end
  end
end
