class AddRatingTypeToRounds < ActiveRecord::Migration
  def change
    add_column :rounds, :rating_type, :integer, default: 0

    # 0 - repeats
    # 1 - time
  end
end
