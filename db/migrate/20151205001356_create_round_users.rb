class CreateRoundUsers < ActiveRecord::Migration
  def change
    create_table :round_users do |t|
      t.references :round, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
