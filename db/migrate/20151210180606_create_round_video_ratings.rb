class CreateRoundVideoRatings < ActiveRecord::Migration
  def change
    create_table :round_video_ratings do |t|
      t.belongs_to :round_video, index: true, foreign_key: true
      t.integer :score
      t.integer :time

      t.timestamps null: false
    end
  end
end
