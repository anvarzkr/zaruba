class AddPriceToRounds < ActiveRecord::Migration
  def change
    add_column :rounds, :start_prize, :integer, default: 1000
    add_column :rounds, :peak_prize, :integer, default: 0
    add_column :rounds, :prize_percent, :integer, default: 100
  end
end
