# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160221162218) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ads", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "site"
    t.boolean  "is_partner"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.boolean  "use_image",          default: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "is_big_picture",     default: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "round_id"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["round_id"], name: "index_comments_on_round_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "competitions", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.date     "start_date"
    t.integer  "days",        default: 7
    t.integer  "timer_days",  default: 3
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "round_tasks", force: :cascade do |t|
    t.string   "title"
    t.integer  "round_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "round_tasks", ["round_id"], name: "index_round_tasks_on_round_id", using: :btree

  create_table "round_users", force: :cascade do |t|
    t.integer  "round_id"
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "available",  default: 1
  end

  add_index "round_users", ["round_id"], name: "index_round_users_on_round_id", using: :btree
  add_index "round_users", ["user_id"], name: "index_round_users_on_user_id", using: :btree

  create_table "round_video_ratings", force: :cascade do |t|
    t.integer  "round_video_id"
    t.integer  "score"
    t.integer  "time"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "round_video_ratings", ["round_video_id"], name: "index_round_video_ratings_on_round_video_id", using: :btree

  create_table "round_videos", force: :cascade do |t|
    t.integer  "round_user_id"
    t.string   "youtube_url"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "minutes",       default: 0
    t.integer  "seconds",       default: 0
    t.integer  "repeats",       default: 0
    t.boolean  "is_checked",    default: false
  end

  add_index "round_videos", ["round_user_id"], name: "index_round_videos_on_round_user_id", using: :btree

  create_table "rounds", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "type_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "competition_id"
    t.integer  "rating_type"
    t.integer  "repeats",            default: 0
    t.integer  "minutes",            default: 0
    t.integer  "seconds",            default: 0
    t.integer  "start_prize",        default: 1000
    t.integer  "peak_prize",         default: 0
    t.integer  "prize_percent",      default: 100
    t.string   "text_prize"
  end

  add_index "rounds", ["competition_id"], name: "index_rounds_on_competition_id", using: :btree
  add_index "rounds", ["type_id"], name: "index_rounds_on_type_id", using: :btree

  create_table "types", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "price"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "is_team",     default: false
    t.integer  "beat_price",  default: 1000
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "fname"
    t.string   "lname"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "role_id"
    t.string   "phone"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["role_id"], name: "index_users_on_role_id", using: :btree

  add_foreign_key "comments", "rounds"
  add_foreign_key "comments", "users"
  add_foreign_key "round_tasks", "rounds"
  add_foreign_key "round_users", "rounds"
  add_foreign_key "round_users", "users"
  add_foreign_key "round_video_ratings", "round_videos"
  add_foreign_key "round_videos", "round_users"
  add_foreign_key "rounds", "competitions"
  add_foreign_key "rounds", "types"
  add_foreign_key "users", "roles"
end
