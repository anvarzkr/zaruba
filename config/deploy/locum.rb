role :app, %w(hosting_chemerisov@titanium.locum.ru)
role :web, %w(hosting_chemerisov@titanium.locum.ru)
role :db, %w(hosting_chemerisov@titanium.locum.ru)

set :ssh_options, forward_agent: true
set :rails_env, :production
