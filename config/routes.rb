Rails.application.routes.draw do
  resources :ads
  resources :competitions
  resources :round_video_ratings
  resources :round_videos
  resources :round_tasks
  resources :comments
  resources :round_users
  resources :rounds
  resources :types
  # devise_for :users

  get '/rounds/type/:type_id' => 'rounds#show_of_type', as: :show_of_type
  get '/round_videos/list/:round_id' => 'round_videos#list', as: :round_video_list
  get '/rounds/rating/:round_id' => 'round_videos#rating', as: :rating_round_video
  get '/categories' => 'types#block_list', as: :categories
  get '/ratings/:type_id' => 'round_video_ratings#list', as: :ratings
  get '/archive' => 'rounds#archive', as: :archive
  get '/archive/:round_id' => 'rounds#archive_round', as: :archive_round

  get '/round_users_payment' => 'round_users#payment'
  post '/round_users_payment' => 'round_users#payment'

  get '/how_to' => 'help#how_to', as: :how_to
  get '/about' => 'help#about', as: :about
  get '/contacts' => 'help#contacts', as: :contacts
  get '/download_reglament' => 'help#download_reglament', as: :download_reglament

  get '/admin' => 'help#admin_panel', as: :admin_panel
  get '/prize' => 'help#prize_panel', as: :prize_panel
  get '/users' => 'help#user_list', as: :user_list

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  root "types#block_list"

  get '/image_crop' => 'help#image_crop', as: :image_crop
  get '/avatar_with_frame' => 'help#avatar_with_frame', as: :avatar_with_frame
  get '/avatar_with_frame_download' => 'help#avatar_with_frame_download', as: :avatar_with_frame_download

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
