require 'test_helper'

class RoundUsersControllerTest < ActionController::TestCase
  setup do
    @round_user = round_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:round_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create round_user" do
    assert_difference('RoundUser.count') do
      post :create, round_user: { round_id: @round_user.round_id, user_id: @round_user.user_id }
    end

    assert_redirected_to round_user_path(assigns(:round_user))
  end

  test "should show round_user" do
    get :show, id: @round_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @round_user
    assert_response :success
  end

  test "should update round_user" do
    patch :update, id: @round_user, round_user: { round_id: @round_user.round_id, user_id: @round_user.user_id }
    assert_redirected_to round_user_path(assigns(:round_user))
  end

  test "should destroy round_user" do
    assert_difference('RoundUser.count', -1) do
      delete :destroy, id: @round_user
    end

    assert_redirected_to round_users_path
  end
end
