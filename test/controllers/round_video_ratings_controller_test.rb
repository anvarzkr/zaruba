require 'test_helper'

class RoundVideoRatingsControllerTest < ActionController::TestCase
  setup do
    @round_video_rating = round_video_ratings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:round_video_ratings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create round_video_rating" do
    assert_difference('RoundVideoRating.count') do
      post :create, round_video_rating: { round_video_id: @round_video_rating.round_video_id, score: @round_video_rating.score, time: @round_video_rating.time }
    end

    assert_redirected_to round_video_rating_path(assigns(:round_video_rating))
  end

  test "should show round_video_rating" do
    get :show, id: @round_video_rating
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @round_video_rating
    assert_response :success
  end

  test "should update round_video_rating" do
    patch :update, id: @round_video_rating, round_video_rating: { round_video_id: @round_video_rating.round_video_id, score: @round_video_rating.score, time: @round_video_rating.time }
    assert_redirected_to round_video_rating_path(assigns(:round_video_rating))
  end

  test "should destroy round_video_rating" do
    assert_difference('RoundVideoRating.count', -1) do
      delete :destroy, id: @round_video_rating
    end

    assert_redirected_to round_video_ratings_path
  end
end
