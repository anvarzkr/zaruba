require 'test_helper'

class RoundVideosControllerTest < ActionController::TestCase
  setup do
    @round_video = round_videos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:round_videos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create round_video" do
    assert_difference('RoundVideo.count') do
      post :create, round_video: { round_user_id: @round_video.round_user_id, youtube_url: @round_video.youtube_url }
    end

    assert_redirected_to round_video_path(assigns(:round_video))
  end

  test "should show round_video" do
    get :show, id: @round_video
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @round_video
    assert_response :success
  end

  test "should update round_video" do
    patch :update, id: @round_video, round_video: { round_user_id: @round_video.round_user_id, youtube_url: @round_video.youtube_url }
    assert_redirected_to round_video_path(assigns(:round_video))
  end

  test "should destroy round_video" do
    assert_difference('RoundVideo.count', -1) do
      delete :destroy, id: @round_video
    end

    assert_redirected_to round_videos_path
  end
end
