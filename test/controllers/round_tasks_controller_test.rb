require 'test_helper'

class RoundTasksControllerTest < ActionController::TestCase
  setup do
    @round_task = round_tasks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:round_tasks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create round_task" do
    assert_difference('RoundTask.count') do
      post :create, round_task: { round_id: @round_task.round_id, title: @round_task.title }
    end

    assert_redirected_to round_task_path(assigns(:round_task))
  end

  test "should show round_task" do
    get :show, id: @round_task
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @round_task
    assert_response :success
  end

  test "should update round_task" do
    patch :update, id: @round_task, round_task: { round_id: @round_task.round_id, title: @round_task.title }
    assert_redirected_to round_task_path(assigns(:round_task))
  end

  test "should destroy round_task" do
    assert_difference('RoundTask.count', -1) do
      delete :destroy, id: @round_task
    end

    assert_redirected_to round_tasks_path
  end
end
