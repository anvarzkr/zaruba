// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require cocoon
//= require_tree .

$(document).ready(function(){

});

function readyFunction() {

  $('.info-spoiler').hide();
  $('.info-spoiler-btn').on('click', function(){
    // $(this).closest('.spoiler-block').find('.info-spoiler').slideToggle(100);

    // 1,2,3

    $(this).closest('.spoiler-block').find('.info-spoiler').slideToggle(100);

    if (/Показать информацию о комплексе/i.test($(this).text()))
      $(this).text("Скрыть" + " информацию о комплексе");
    else if (/Скрыть информацию о комплексе/i.test($(this).text()))
      $(this).text("Показать" + " информацию о комплексе");
    else if (/отправка видео/i.test($(this).text()))
      $(this).text("Скрыть форму отправки видео");
    else if (/скрыть форму отправки видео/i.test($(this).text()))
      $(this).text("Отправка видео");
  });

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

};

function turbolinksSetInterval(intervalFunction, seconds) {
  console.log("hey");
  var interval = setInterval(intervalFunction, seconds);
  $(document).on('page:change', removeInterval);

  function removeInterval() {
    console.log("hey clear");
    clearInterval(interval);
    $(document).off('page:change', removeInterval);
  }
}

// $(document).ready(ready);
// $(document).on('page:load', ready);

// var currentDate = new Date();
// var endDate = new Date(2015, 11, 13, 23, 59, 59, 0);
// var milliseconds = endDate.getTime() - currentDate.getTime();
// //$('#timer').html(endDate.getDate() + " " + currentDate.getDate());
// setInterval(function(){
//   var timeLeft = new Date( milliseconds );
//   $('#timer').html(timeLeft.getDate() + ((timeLeft.getDate() == 1) ? " день " : " дня ") + timeLeft.getHours() + " часа " + timeLeft.getMinutes() + " минут" + '<p class="timer-seconds">' + timeLeft.getSeconds() + ' секунд</p>');
//   milliseconds -= 1000;
// }, 1000);
