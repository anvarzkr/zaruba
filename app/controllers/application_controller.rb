class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :get_youtube_link, :getAds, :getPartners

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def is_url_valid?(url)
    require 'uri'
    !!url =~ URI::regexp
  end

  def get_youtube_link(url)
    if url.include? '?v='
      return "#{request.protocol}www.youtube.com/embed/#{url.split('?v=').last}"
    else
      return "#{request.protocol}www.youtube.com/embed/#{url.split('/').last}"
    end
  end

  def getAds(adsCount)
    Ad.where(is_partner: false).order("RANDOM()").limit(adsCount)
  end

  def getPartners(adsCount)
    Ad.where(is_partner: true).order("RANDOM()").limit(adsCount)
  end

  def admin_required!
    redirect_to :root if authenticate_user! && !current_user.is_admin?
  end

end
