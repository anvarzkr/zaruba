class RoundVideosController < ApplicationController
  before_action :set_round_video, only: [:show, :edit, :update, :destroy]
  before_action :admin_required!, except: [:show, :create]

  # GET /round_videos
  # GET /round_videos.json
  def index
    @round_videos = RoundVideo.order(created_at: :desc, is_checked: :asc).select { |rv| rv.round_user != nil }

    # abort(@round_videos.inspect)
  end

  def list
    @round_id = params[:round_id]
    if @round_id && @round_id != '0' && @round_id != '00'
      @round_videos =  RoundVideo.order(created_at: :desc, is_checked: :asc).joins(:round_user).where(round_users: {round_id: @round_id})
      @round_id = @round_video.nil? || @round_video.first.nil? ? @round_id.sub(/^0/, "").to_i : @round_video.first.round_user.round_id
    else
      @round_videos = RoundVideo.order(created_at: :desc, is_checked: :asc).select { |rv| rv.round_user != nil }
      @round_id = 0;
    end

    @rounds = Round.all

    # abort(@round_videos)
  end

  # GET /round_videos/1
  # GET /round_videos/1.json
  def show
  end

  # GET /round_videos/new
  def new
    @round_video = RoundVideo.new
  end

  # GET /round_videos/1/edit
  def edit
  end

  def rating
    @round_video = RoundVideo.find(params[:round_id])
    @round_video_rating = @round_video.round_video_rating
    @round_video_rating ||= RoundVideoRating.new(round_video_id: @round_video.id)
  end

  # POST /round_videos
  # POST /round_videos.json
  def create
    @round_video = RoundVideo.new(round_video_params)
    # @round_video.user_id = current_user.id unless current_user.is_admin?

    round_user = @round_video.round_user

    if round_user.available == 0
      return redirect_to show_of_type_path(@round_video.round_user.round.type_id)
    end

    round_user.available -= 1
    round_user.save

    round = @round_video.round_user.round

    if round.rating_type == 0
      @round_video.minutes = round.minutes
      @round_video.seconds = round.seconds
    elsif round.rating_type == 1
      @round_video.repeats = round.repeats
    end

    respond_to do |format|
      if @round_video.save
        format.html { redirect_to show_of_type_path(@round_video.round_user.round.type_id), notice: "Видео было успешно отправлено!" }
        format.json { render :show, status: :created, location: @round_video }
      else
        format.html { redirect_to show_of_type_path(@round_video.round_user.round.type_id) }
        format.json { render json: @round_video.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /round_videos/1
  # PATCH/PUT /round_videos/1.json
  def update
    respond_to do |format|
      if @round_video.update(round_video_params)
        format.html { redirect_to round_video_list_path(0), notice: 'Round video was successfully updated.' }
        format.json { render :show, status: :ok, location: @round_video }
      else
        format.html { redirect_to round_video_list_path(0), notice: 'Round video was unsuccessfully updated.' }
        format.json { render json: @round_video.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /round_videos/1
  # DELETE /round_videos/1.json
  def destroy
    @round_video.destroy
    respond_to do |format|
      format.html { redirect_to round_video_list_path(0), notice: 'Round video was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_round_video
      @round_video = RoundVideo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def round_video_params
      params.require(:round_video).permit(:round_user_id, :youtube_url, :minutes, :seconds, :repeats, :is_checked)
    end
end
