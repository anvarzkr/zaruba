class HelpController < ApplicationController
  before_action :admin_required!, only: [:admin_panel, :prize_panel, :user_list]

  # require 'rubygems'
  require 'RMagick'
  include Magick

  def about
  end

  def how_to
  end

  def download_reglament
    send_file ("public/zaruba-reglament.docx"), :type=>"application/vnd.openxmlformats-officedocument.wordprocessingml.document", :x_sendfile=>true
  end

  def contacts
  end

  def admin_panel
  end

  def prize_panel
  end

  def user_list
    @users = User.all.order(created_at: :asc)

    @last_month_users_count = @users.select{ |u| u.created_at >= Date.today.at_beginning_of_month }.count
  end

  def image_crop
    frame = Image.read('public/frame.png')[0]
    avatar = Image.read('public/avatar.png')[0]
    frame = frame.adaptive_resize(avatar.columns, avatar.rows)
    # avatar = avatar.adaptive_resize(600, 600)
    avatar = avatar.crop((avatar.columns - (avatar.rows.to_f * 4/5)) / 2, 0, (avatar.rows.to_f * 4/5), avatar.rows)
    frame = frame.adaptive_resize(avatar.columns, avatar.rows)
    combined = avatar.composite(frame, Magick::EastGravity, 0, 0, Magick::OverCompositeOp) #the 0,0 is the x,y
    combined.format = 'png'
    send_data combined.to_blob, :stream => 'false', :filename => 'zaruba_avatar.png', :type => 'image/png', :disposition => 'inline'

    # processed_image = StringIO.open(trimmed_img.to_blob)
  end

  def image_test_work
    frame = Image.read('public/frame.png')[0]
    avatar = Image.read('public/avatar.png')[0]
    avatar = avatar.adaptive_resize(636, 578)
    combined = frame.composite(avatar, Magick::EastGravity, 272, -76, Magick::OverCompositeOp) #the 0,0 is the x,y
    combined.format = 'png'
    send_data combined.to_blob, :stream => 'false', :filename => 'zaruba_avatar.png', :type => 'image/png', :disposition => 'inline'
  end

  def avatar_with_frame
    frame = Image.read('public/frame.png')[0]

    begin
      if current_user.avatar.url.include? "missing_avatar"
        avatar = Image.read('public/' + 'missing_avatar.png')[0]
      else
        avatar = Image.read('public' + current_user.avatar.url(:medium).split('?')[0])[0]
      end
    rescue => ex
      avatar = Image.read('public/' + 'missing_avatar.png')[0]
    end

    frame = frame.adaptive_resize(avatar.columns, avatar.rows)
    combined = avatar.composite(frame, Magick::EastGravity, 0, 0, Magick::OverCompositeOp) #the 0,0 is the x,y
    combined.format = 'png'
    send_data combined.to_blob, :stream => 'false', :filename => 'zaruba_avatar.png', :type => 'image/png', :disposition => 'inline'
  end

  def avatar_with_frame_download
    cookies.permanent[:avatar_downloaded] = "true"

    frame = Image.read('public/frame.png')[0]

    begin
      if current_user.avatar.url.include? "missing_avatar"
        avatar = Image.read('public/' + 'missing_avatar.png')[0]
      else
        avatar = Image.read('public' + current_user.avatar.url(:medium).split('?')[0])[0]
      end
    rescue => ex
      avatar = Image.read('public/' + 'missing_avatar.png')[0]
    end

    frame = frame.adaptive_resize(avatar.columns, avatar.rows)
    combined = avatar.composite(frame, Magick::EastGravity, 0, 0, Magick::OverCompositeOp) #the 0,0 is the x,y
    combined.format = 'png'
    send_data combined.to_blob, :filename => 'zaruba_avatar.png', :disposition => 'attachment'
  end

end
