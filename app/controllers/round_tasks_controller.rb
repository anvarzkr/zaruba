class RoundTasksController < ApplicationController
  before_action :set_round_task, only: [:show, :edit, :update, :destroy]
  before_action :admin_required!

  # GET /round_tasks
  # GET /round_tasks.json
  def index
    @round_tasks = RoundTask.all
  end

  # GET /round_tasks/1
  # GET /round_tasks/1.json
  def show
  end

  # GET /round_tasks/new
  def new
    @round_task = RoundTask.new
  end

  # GET /round_tasks/1/edit
  def edit
  end

  # POST /round_tasks
  # POST /round_tasks.json
  def create
    @round_task = RoundTask.new(round_task_params)

    respond_to do |format|
      if @round_task.save
        format.html { redirect_to @round_task, notice: 'Round task was successfully created.' }
        format.json { render :show, status: :created, location: @round_task }
      else
        format.html { render :new }
        format.json { render json: @round_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /round_tasks/1
  # PATCH/PUT /round_tasks/1.json
  def update
    respond_to do |format|
      if @round_task.update(round_task_params)
        format.html { redirect_to @round_task, notice: 'Round task was successfully updated.' }
        format.json { render :show, status: :ok, location: @round_task }
      else
        format.html { render :edit }
        format.json { render json: @round_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /round_tasks/1
  # DELETE /round_tasks/1.json
  def destroy
    @round_task.destroy
    respond_to do |format|
      format.html { redirect_to round_tasks_url, notice: 'Round task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_round_task
      @round_task = RoundTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def round_task_params
      params.require(:round_task).permit(:title, :round_id)
    end
end
