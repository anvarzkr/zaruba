class RoundsController < ApplicationController
  before_action :set_round, only: [:show, :edit, :update, :destroy]
  before_action :admin_required!, except: [:show_of_type, :archive, :archive_round]

  # GET /rounds
  # GET /rounds.json
  def index
    @rounds = Round.all.order(id: :desc)
  end

  # GET /rounds/1
  # GET /rounds/1.json
  # def show
  # end

  # GET /rounds/types/1
  def show_of_type
    @need_round_types = true

    # abort((Time.now.end_of_day + 1.second).inspect)

    @competition = Competition.current
    return redirect_to root_path if @competition.nil?

    # @round = Round.where(type_id: params[:type_id]).first or not_found
    @round = @competition.rounds.select { |r| r.type_id.to_s == params[:type_id].to_s }.first

    return redirect_to root_path if @round.nil?

    # abort(@round.inspect)

    @round_users_count = RoundUser.where(round_id: @round.id).count
    @prize = @round.type.price * @round_users_count * 0.7
    # if @round_users_count == 1
    #   @prize = @round.type.price
    # else
    #   @prize = @round.type.price * @round_users_count * 0.7
    # end

    if current_user
      # @competition.round.each do |r|
      #   if r.type_id == 5
      # end
      @competition_user_rounds = current_user.round_users.joins(:round).where({rounds: { competition_id: @competition.id}})
      @team_round_registered = false
      @not_team_round_registered = false

      @team_category = Type.find_by_is_team(true)

      @competition_user_rounds.each do |cur|
        if !@team_category.nil? && cur.round.type_id == @team_category.id
          @team_round_registered = true
        else
          @not_team_round_registered = true
        end
      end

      @cant_register_team = @team_round_registered
      @cant_register_solo = @not_team_round_registered

      # abort(@cant_register.inspect)

      # @user_round_registered = !current_user.round_users.find_by(round_id: @round.id).blank?
      @round_user = current_user.round_users.find_by(round_id: @round.id)
      # @round_video_sent = !@round_user.round_videos.blank? if @round_user
      @round_video_sent = @round_user.available == 0 if @round_user
    end
    @comments = @round.comments
    @round_tasks = RoundTask.where(round_id: @round.id)

    @end_date = (@round.competition.start_date + @round.competition.days.days - 1.days).in_time_zone('Moscow').end_of_day

    # abort @end_date.inspect
    # abort Time.now.inspect

    if @end_date < Time.now

      if @round_user
        if @round_user.round_videos.select{|rv| rv.created_at < @end_date}.count == 0
          if @round_user.available - 1 > 0
            @can_send_video = true
          end
        elsif @round_user.available > 0
          @can_send_video = true
        end
      end

      @end_date = (@end_date + @round.competition.timer_days.days).in_time_zone('Moscow').end_of_day

      # abort @end_date.inspect

      @winner_user_enabled = true

      @round_videos = RoundVideo.joins(:round_user).where(is_checked: true, round_users: { round_id: @round.id })

      if @round.rating_type == 0
        @winner_round_video = @round_videos.order(repeats: :desc).first
      else
        @winner_round_video = @round_videos.order(minutes: :asc, seconds: :asc).first
      end
    end
  end

  def archive
    @rounds = Round.all.order(id: :desc)
  end

  def archive_round
    @archive_round = true
    @round = Round.find(params[:round_id])

    @round_users_count = RoundUser.where(round_id: @round.id).count
    if @round_users_count == 1
      @prize = @round.type.price
    else
      @prize = @round.type.price * @round_users_count * 0.7
    end

    @competition = Competition.current
    return redirect_to root_path if @competition.nil?

    @round_videos = RoundVideo.joins(:round_user).where(round_users: { round_id: @round.id }).order(repeats: :desc, minutes: :asc, seconds: :asc)
    @winner_round_video = @round_videos.first
  end

  # GET /rounds/new
  def new
    @round = Round.new
  end

  # GET /rounds/1/edit
  def edit
  end

  # POST /rounds
  # POST /rounds.json
  def create
    @round = Round.new(round_params)

    respond_to do |format|
      if @round.save
        format.html { redirect_to rounds_path, notice: 'Round was successfully created.' }
        format.json { render :show, status: :created, location: @round }
      else
        format.html { render :new }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rounds/1
  # PATCH/PUT /rounds/1.json
  def update
    respond_to do |format|
      if @round.update(round_params)
        format.html { redirect_to rounds_path, notice: 'Round was successfully updated.' }
        format.json { render :show, status: :ok, location: @round }
      else
        format.html { render :edit }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rounds/1
  # DELETE /rounds/1.json
  def destroy
    @round.destroy
    respond_to do |format|
      format.html { redirect_to rounds_url, notice: 'Round was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_round
      @round = Round.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def round_params
      params.require(:round).permit(:title, :description, :type_id, :image, :competition_id, :rating_type, :repeats, :minutes, :seconds, :start_prize, :peak_prize, :prize_percent, :text_prize, round_tasks_attributes: [:id, :title, :_destroy])
    end
end
