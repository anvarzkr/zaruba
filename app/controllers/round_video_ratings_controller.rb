class RoundVideoRatingsController < ApplicationController
  before_action :set_round_video_rating, only: [:show, :edit, :update, :destroy]
  before_action :admin_required!, except: [:list]

  # GET /round_video_ratings
  # GET /round_video_ratings.json
  def index
    @round_video_ratings = RoundVideoRating.all
  end

  # GET /round_video_ratings/1
  # GET /round_video_ratings/1.json
  def show
  end

  def list
    type_id = params[:type_id]
    @need_rating_types = true

    @competition = Competition.current
    return redirect_to root_path if @competition.nil?

    @round = @competition.rounds.select { |r| r.type_id.to_s == type_id.to_s }.first
    return redirect_to root_path if @round.nil?

    @round_videos = RoundVideo.joins(:round_user).where(round_users: { round_id: @round.id })

    if @round.rating_type == 0
      @round_videos = @round_videos.order(repeats: :desc)
    else
      @round_videos = @round_videos.order(minutes: :asc, seconds: :asc)
    end

    exist_ids = []

    @round_videos = @round_videos.select do |rv|
      unless exist_ids.include? rv.round_user_id
        exist_ids.push(rv.round_user_id)
        rv
      end
    end

    # abort(@round_videos.inspect)



    # @r = params[:type_id];
    # @round_video_ratings = RoundVideoRating.all.collect { |rvr| rvr if rvr.round_video.round_user.round.type_id == params[:type_id] }
    # @round_video_ratings = RoundVideoRating.all
    # @round_video_ratings.collect! { |rvr| rvr if rvr.round_video.round_user.round.type_id == params[:type_id] }
    # @round_video_ratings = RoundVideoRating.all.collect { |rvr| rvr.round_video.round_user.round.type_id == params[:type_id] } || []
  end

  # GET /round_video_ratings/new
  def new
    @round_video_rating = RoundVideoRating.new
  end

  # GET /round_video_ratings/1/edit
  def edit
  end

  # POST /round_video_ratings
  # POST /round_video_ratings.json
  def create
    @round_video_rating = RoundVideoRating.new(round_video_rating_params)

    respond_to do |format|
      if @round_video_rating.save
        format.html { redirect_to @round_video_rating, notice: 'Round video rating was successfully created.' }
        format.json { render :show, status: :created, location: @round_video_rating }
      else
        format.html { render :new }
        format.json { render json: @round_video_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /round_video_ratings/1
  # PATCH/PUT /round_video_ratings/1.json
  def update
    respond_to do |format|
      if @round_video_rating.update(round_video_rating_params)
        format.html { redirect_to @round_video_rating, notice: 'Round video rating was successfully updated.' }
        format.json { render :show, status: :ok, location: @round_video_rating }
      else
        format.html { render :edit }
        format.json { render json: @round_video_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /round_video_ratings/1
  # DELETE /round_video_ratings/1.json
  def destroy
    @round_video_rating.destroy
    respond_to do |format|
      format.html { redirect_to round_video_ratings_url, notice: 'Round video rating was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_round_video_rating
      @round_video_rating = RoundVideoRating.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def round_video_rating_params
      params.require(:round_video_rating).permit(:round_video_id, :score, :time)
    end
end
