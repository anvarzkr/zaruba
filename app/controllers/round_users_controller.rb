class RoundUsersController < ApplicationController
  before_action :set_round_user, only: [:show, :edit, :update, :destroy]
  before_action :admin_required!, except: [:payment]

  protect_from_forgery with: :null_session,
      if: Proc.new { |c| c.request.format =~ %r{application/x-www-form-urlencoded} }

  # GET /round_users
  # GET /round_users.json
  def index
    @round_users = RoundUser.all
  end

  # GET /round_users/1
  # GET /round_users/1.json
  def show
  end

  # GET /round_users/new
  def new
    @round_user = RoundUser.new
  end

  # GET /round_users/1/edit
  def edit
  end

  # POST /round_users/payment
  def payment
    label = params[:label]

    return redirect_to root_path if label.nil?

    label = label.split('-')

    withdraw_amount = params[:withdraw_amount].to_i

    return redirect_to root_path if withdraw_amount.nil?

    user_id = label[0].to_i
    round_id = label[1].to_i

    round = Round.find(round_id)

    if withdraw_amount == round.type.price || withdraw_amount == round.type.beat_price
      round_user = RoundUser.where(user_id: user_id, round_id: round_id).first

      if round_user

        round_user.available += 1

        if round_user.save
          UserMailer.round_payed_email(round_user.user, round_user.round).deliver_now
        end

        logger.debug "RoundUser is found."
      else
        round_user = RoundUser.create({user_id: user_id, round_id: round_id})
        # UserMailer.round_payed_email(round_user.user, round_user.round).deliver_now
      end

      logger.debug "withdraw_amount is equals to #{round.type.price}."
    end

    logger.debug "user_id: #{user_id}, round_id: #{round_id}, withdraw_amount: #{withdraw_amount}"
    # logger.debug params[:notification_secret]
    logger.debug params.inspect

    # if round_user && round_user.round
    #   return redirect_to round_user.round
    # else
    #   return redirect_to root_path
    # end
  end

  # POST /round_users
  # POST /round_users.json
  def create
    # @round_user = RoundUser.new(round_user_params)
    @round_user = RoundUser.where(round_id: round_user_params[:round_id], user_id: current_user.id).first
    @round_user ||= current_user.round_users.new(round_id: round_user_params[:round_id])

    respond_to do |format|
      if @round_user.save
        format.html { redirect_to @round_user, notice: 'Round user was successfully created.' }
        format.js {}
        format.json { render :show, status: :created, location: @round_user }
      else
        format.html { render :new }
        format.json { render json: @round_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /round_users/1
  # PATCH/PUT /round_users/1.json
  def update
    respond_to do |format|
      if @round_user.update(round_user_params)
        format.html { redirect_to @round_user, notice: 'Round user was successfully updated.' }
        format.json { render :show, status: :ok, location: @round_user }
      else
        format.html { render :edit }
        format.json { render json: @round_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /round_users/1
  # DELETE /round_users/1.json
  def destroy
    @round_user.destroy
    respond_to do |format|
      format.html { redirect_to round_users_url, notice: 'Round user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_round_user
      @round_user = RoundUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def round_user_params
      params.require(:round_user).permit(:round_id, :user_id)
    end
end
