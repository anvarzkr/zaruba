# /app/helpers/devise_helper.rb

module DeviseHelper
  def devise_error_messages!
    return '' if resource.errors.empty?

    # messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join

    errors_count = 0

    messages = resource.errors.messages.map do |msg|
      errors_count += 1
      msg[1][0] = "Поле 'Email' " + msg[1][0] if msg[1][0] == "не может быть пустым" && msg[0] == :email
      msg[1][0] = "Пароль " + msg[1][0] if msg[0] == :password || msg[0] == :password_confirmation
      content_tag(:li, msg[1][0])
    end

    messages = messages.join

    # abort(messages.inspect)

    # sentence = I18n.t('errors.messages.not_saved',
    #   count: resource.errors.count,
    #   resource: resource.class.model_name.human.downcase)

    sentence = "Сохранение не удалось из-за #{errors_count} " + ((errors_count == 1) ? "ошибки" : "ошибок")

    html = <<-HTML
    <div class="alert fade in alert-danger ">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <h4>#{sentence}</h4>
      #{messages}
    </div>
    HTML

    html.html_safe
  end
end
