class ApplicationMailer < ActionMailer::Base
  default from: "robot@online-zaruba.com"
  layout 'mailer'
end
