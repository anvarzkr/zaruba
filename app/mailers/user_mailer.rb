class UserMailer < ApplicationMailer

  def round_payed_email(user, round)
    @user = user
    # @url  = 'http://online-zaruba.com/login'
    @round = round
    mail(to: @user.email, subject: 'Welcome to My Awesome Site')
  end

end
