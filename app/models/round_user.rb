class RoundUser < ActiveRecord::Base
  belongs_to :round
  belongs_to :user
  has_many :round_videos, :dependent => :nullify
end
