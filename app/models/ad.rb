class Ad < ActiveRecord::Base

  has_attached_file :image, styles: { medium: "260x300#", thumb: "260x41#" },
    default_url: ":style/missing_ad.png",
    url: "/system/rek/:id/:style/:basename.:extension",
    path: ":rails_root/public/system/rek/:id/:style/:basename.:extension",
    hash_secret: "longSecretString"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

end
