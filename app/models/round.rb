class Round < ActiveRecord::Base
  belongs_to :type
  belongs_to :competition
  has_many :round_users, :dependent => :nullify
  has_many :comments, :dependent => :delete_all
  has_many :round_tasks, :dependent => :delete_all
  accepts_nested_attributes_for :round_tasks, reject_if: :all_blank, allow_destroy: true

  has_attached_file :image, styles: { high: "500x700>", medium: "300x450>", thumb: "100x170>" },
    default_url: ":style/missing_round_image.png"
    # url: "/system/rounds/:id/:style/:basename.:extension",
    # path: ":rails_root/public/system/rounds/:id/:style/:hash.:extension",
    # hash_secret: "longSecretString"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  def prize
    prize = (self.start_prize + self.round_users.count * self.type.price * (self.prize_percent.to_f / 100)).round

    if self.peak_prize <= 0
      return prize
    else
      return [prize, self.peak_prize].min.round
    end
  end

end
