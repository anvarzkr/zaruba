class Competition < ActiveRecord::Base
  has_many :rounds, :dependent => :nullify

  def is_active?
    (start_date + (days + timer_days - 1).days).in_time_zone('Moscow').end_of_day > Time.now
    # abort ((start_date + (days + timer_days).days) - 1.second).inspect
    # abort (start_date + (days + timer_days - 1).days).in_time_zone('Moscow').end_of_day.inspect
    # abort (Time.now).inspect
    # abort (start_date + (days + timer_days - 1).days).in_time_zone('Moscow').end_of_day > Time.now
  end

  def self.current
    return Competition.where(["start_date <= ?", Time.now.end_of_day]).order(start_date: :desc).first
  end
end
