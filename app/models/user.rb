class User < ActiveRecord::Base
  has_many :round_users
  has_many :comments
  belongs_to :role
  before_create :set_default_role

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :avatar, styles: { medium: "300x375#", thumb: "300x300#" },
    default_url: ":style/missing_avatar.png"
    # url: "/system/users/:id/:style/:basename.:extension",
    # path: ":rails_root/public/system/users/:id/:style/:hash.:extension",
    # hash_secret: "longSecretString"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  validates_acceptance_of :user_agreement

  validates :fname, presence: {message: "Поле 'Имя' обязательно для заполнения!"}
  validates :lname, presence: {message: "Поле 'Фамилия' обязательно для заполнения!"}
  validates :email, presence: {message: "Поле 'Email' обязательно для заполнения!"},
    email_format: { message: "Поле 'Email' имеет недопустимое значение!" }, email: true
  validates :phone,
    presence: {:message => "Поле 'Телефон' обязательно для заполнения!"},
    length: { in: 10..11, too_short: "Телефон должен содержать не менее 10 цифр", too_long: "Телефон должен содержать не более 11 цифр" },
    numericality: { only_integer: true, message: "Телефон может содержать только цифры" }

  def is_admin?
    return self.role.name == 'admin'
  end

  private
  def set_default_role
    self.role ||= Role.find_by_name('registered')
  end

end
