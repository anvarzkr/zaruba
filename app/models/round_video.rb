class RoundVideo < ActiveRecord::Base
  belongs_to :round_user
  has_one :round_video_rating, :dependent => :delete
end
