json.array!(@types) do |type|
  json.extract! type, :id, :name, :description, :price
  json.url type_url(type, format: :json)
end
