json.array!(@round_tasks) do |round_task|
  json.extract! round_task, :id, :title, :round_id
  json.url round_task_url(round_task, format: :json)
end
