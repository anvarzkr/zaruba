json.array!(@rounds) do |round|
  json.extract! round, :id, :title, :description, :type_id
  json.url round_url(round, format: :json)
end
