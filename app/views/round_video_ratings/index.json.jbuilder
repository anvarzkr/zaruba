json.array!(@round_video_ratings) do |round_video_rating|
  json.extract! round_video_rating, :id, :round_video_id, :score, :time
  json.url round_video_rating_url(round_video_rating, format: :json)
end
