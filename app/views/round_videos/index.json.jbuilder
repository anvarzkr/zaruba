json.array!(@round_videos) do |round_video|
  json.extract! round_video, :id, :round_user_id, :youtube_url
  json.url round_video_url(round_video, format: :json)
end
