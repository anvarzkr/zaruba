json.array!(@competitions) do |competition|
  json.extract! competition, :id, :title, :description, :start_date, :days, :timer_days
  json.url competition_url(competition, format: :json)
end
