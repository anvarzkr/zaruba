json.array!(@round_users) do |round_user|
  json.extract! round_user, :id, :round_id, :user_id
  json.url round_user_url(round_user, format: :json)
end
